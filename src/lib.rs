//! library for efficiently picking cards from a deck
//! most effecient when picking a small number of items from a large deck

mod common;
mod int;
mod range;
mod slice;
pub mod prelude;

pub use crate::common::{Deck, ToDeck, Rng};
pub use crate::int::IntDeck;
pub use crate::range::RangeDeck;
pub use crate::slice::SliceDeck;

#[cfg(test)]
mod tests {
    use super::*;

	fn default_rng() -> rand_core::OsRng {
		return rand_core::OsRng;
	}
	
    #[test]
    fn pick_int() {
        let mut deck = IntDeck::new(20, default_rng());
		while deck.left() > 0 {
			let a = deck.draw();
			let b = deck.draw();
			println!("{}, {}", a, b); 
			assert!(a != b);
		}
    }
	#[test]
	fn pick_range() {
		let mut deck = (10..20).to_deck(default_rng());
		for _ in 0..5 {
			let a = deck.draw();
			let b = deck.draw();
			println!("{}, {}", a, b); 
			assert!(a != b);
		}
		assert_eq!(deck.try_draw(), None);
	}
	#[test]
	fn pick_slice() {
		let mut deck = [10, 20, 30, 40].to_deck(default_rng());
		while deck.left() > 0 {
			let a = deck.draw();
			let b = deck.draw();
			assert!(a != b);
		}
	}

	#[test]
	fn int_uniform() {
		const NUM_TRIALS: usize = 100000;
		const DECK_SIZE: usize = 15;
		const ALLOWED_DIFF: usize = 500;
		let mut deck = (0..DECK_SIZE).to_deck(default_rng());
		let mut tally = [0_usize; DECK_SIZE];
		
		for _ in 0..NUM_TRIALS {
			deck.refill();
			let a = deck.draw();
			let b = deck.draw();
			assert!(a != b);
			tally[a] += 1;
			tally[b] += 1;
		}
		println!("tally: {:?}", tally);
		let max = tally.iter().max_by(|a, b| a.cmp(b)).unwrap();
		let min = tally.iter().min_by(|a, b| a.cmp(b)).unwrap();
		println!("diff: {}", max-min);
		// unfortunatly i don't know stats, so i can't do proper standard deviation stuff.
		assert!(max-min < ALLOWED_DIFF);
	}
}

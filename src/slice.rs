use crate::prelude::*;

/// a [`Deck`] that randomly draws elements from a slice.
///
/// note that this works by randomly picking an *index* of the slice,
/// so it is possible to draw two card with equal value if (and only if)
/// the slice contains two elements of equal value.
///
/// [`Deck`]: trait.Deck.html 
pub struct SliceDeck<'a, R: Rng, T> {
	base: IntDeck<R>,
	slice: &'a [T],
}

impl<'a, R: Rng, T> Deck for SliceDeck<'a, R, T> {
	type Card = &'a T;
	fn left(&self) -> usize { self.base.left() }
	fn draw(&mut self) -> &'a T { &self.slice[self.base.draw()] }
	fn refill(&mut self) { self.base.refill() }
}


impl<'a, R: Rng, T> ToDeck<R> for &'a [T] {
	type Out = SliceDeck<'a, R, T>;
	fn to_deck(self, r: R) -> Self::Out {
		Self::Out {
			base: IntDeck::new(self.len(), r),
			slice: self,
		}
	}
}

pub use rand_core::RngCore as Rng;

/// a Deck abstractly represents a source of distinct random elements.
pub trait Deck {
	type Card;
	/// number of remaining cards in the deck.
	fn left(&self) -> usize;
	/// draw a random card from the deck.
	///
	/// panics if there are no more cards in the deck.
	fn draw(&mut self) -> Self::Card;
	/// add all previously drawn cards back into the deck.
	fn refill(&mut self);
	/// draw a random card from the deck, returning None if the deck is empty.
	fn try_draw(&mut self) -> Option<Self::Card> {
		if self.left() > 0 { Some(self.draw()) } else { None }
	}
}

/// convert a container-like type into a [`Deck`] that can be drawn from.
///
/// [`Deck`]: trait.Deck.html
pub trait ToDeck<R: Rng> {
	type Out: Deck;
	/// construct a [`Deck`] using the given rng.
	///
	/// [`Deck`]: trait.Deck.html
	fn to_deck(self, r: R) -> Self::Out;
}

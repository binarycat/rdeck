use crate::prelude::*;
use std::ops::Range;

/// a [`Deck`] that randomly draws elements from a [`Range`].
///
/// [`Deck`]: trait.Deck.html
/// [`Range`]: https://doc.rust-lang.org/std/ops/struct.Range.html.html
pub struct RangeDeck<R: Rng> {
	shift: usize,
	base: IntDeck<R>,
}

impl<R: Rng> Deck for RangeDeck<R> {
	type Card = usize;
	fn left(&self) -> usize { self.base.left() }
	fn draw(&mut self) -> usize { self.base.draw() + self.shift }
	fn refill(&mut self) { self.base.refill() }
}

impl<R: Rng> ToDeck<R> for Range<usize> {
	type Out = RangeDeck<R>;

	fn to_deck(self, r: R) -> Self::Out {
		RangeDeck{
			shift: self.start,
			base: IntDeck::new(self.end-self.start, r),
		}
	}
}

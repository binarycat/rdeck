use rdeck::{Deck, ToDeck, Rng};
use rand_core::OsRng;

fn make_playing_deck(values: &[&str], suits: &[&str]) -> Vec<String> {
	let mut r = Vec::with_capacity(values.len() * suits.len());
	for v in values {
		for s in suits {
			r.push(format!("{} of {}", v, s));
		}
	}
	return r;
}

const SUITS: &[&str] = &["diamonds", "clubs", "hearts", "spades"];
const VALUES: &[&str] = &[
	"ace", "two", "three", "four", "five", "six", "seven", "eight", "nine",
	"ten", "jack", "queen", "king"];

fn main() {
	let count: usize = std::env::args().nth(1)
		.expect("expected argument: number of cards to draw")
		.parse().expect("cannot draw non-integer number of cards");
	let cards = make_playing_deck(VALUES, SUITS);
	let mut deck = cards.as_slice().to_deck(OsRng);
	for _ in 0..count {
		if let Some(card) = deck.try_draw() {
			println!("{}", card);
		} else {
			eprintln!("deck is out of cards!");
			std::process::exit(2);
		}
	}
}
